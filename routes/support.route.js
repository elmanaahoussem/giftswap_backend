const express = require('express');
const router = express.Router();
const AuthGuard = require('../classes/AuthGuard');
const AdminGuard = require('../classes/AdminGuard');
 
const support_controller = require('../controllers/support.controller');
 

router.post('/ticket', AuthGuard, support_controller.create);
router.post('/ticket/:id/reply', AuthGuard, support_controller.reply);
router.get('/unsolvedtickets', AdminGuard, support_controller.unsolvedtickets); 
router.get('/solvedtickets', AdminGuard, support_controller.solvedtickets);   
router.get('/alltickets', AdminGuard, support_controller.get_all_tickets);  
router.get('/ticket/:id', AuthGuard, support_controller.get_ticket); 
router.get('/mytickets/', AuthGuard, support_controller.get_user_tickets); 
router.put('/ticket/:id/solve', AuthGuard, support_controller.solveTicket); 

router.put('/:id/update', support_controller.update_article);   
router.post('/contact', support_controller.contact); 
router.delete('/ticket/delete/:id',support_controller.delete_ticket);

module.exports = router;