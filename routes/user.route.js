const express = require('express');
const router = express.Router();
const AdminGuard = require('../classes/AdminGuard')
 
const user_controller = require('../controllers/user.controller');
 
router.post('/add', user_controller.add_user); 
router.post('/admin/add', AdminGuard, user_controller.add_admin)
router.get('/admin', user_controller.get_all_admin);
router.get('/admin/login', user_controller.get_admin)
router.get('/verify/:token', user_controller.verify_user);

router.get('', user_controller.get_all_users);
router.put('/:id/update', user_controller.update_user);
router.delete('/delete/:id', user_controller.delete_user);
router.get('/:id', AdminGuard, user_controller.get_user);

router.post('/register', user_controller.add_user); 
router.post('/login', user_controller.login)
router.post('/refresh-token', user_controller.refresh)
router.get('/verify_email', user_controller.verify_email)
router.post('/request-password-reset', user_controller.request_password_reset)
router.post('/reset-password', user_controller.reset_password)
router.post('/resend-confirmation', user_controller.resend_confirmation)

module.exports = router;