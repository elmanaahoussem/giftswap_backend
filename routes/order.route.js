const express = require('express');
const AuthGuard = require('../classes/AuthGuard');
const AdminGuard = require('../classes/AdminGuard');
const router = express.Router();

const order_controller = require('../controllers/order.controller');

router.post('/add', order_controller.add);
router.get('/list', AdminGuard, order_controller.get_all_orders);
router.get('/myorders', AuthGuard, order_controller.myorders);
router.get('/by-user/:id', AuthGuard, order_controller.get_user_orders); 
router.get('/:id', AuthGuard, order_controller.get_order); 
router.post('/checkout', AuthGuard,order_controller.checkout);
router.put('/:id/cancel', AuthGuard, order_controller.cancel);
router.get('/:id/processing', order_controller.processing);
router.post('/:id/completed', order_controller.completed);
router.post('/:id/redeem', AdminGuard, order_controller.redeem);
router.post('/:id/pay', AuthGuard, order_controller.pay);

module.exports = router;