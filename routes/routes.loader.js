const express = require('express'); 
const cors = require('cors'); 
const bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');

const user = require('./user.route');   
const order = require('./order.route');  
const support = require('./support.route');
const countries = require('./countries.route');
const { Mailer } = require('../classes/Mailer');
const { DEV } = require('../classes/Env');
const { posts } = require('./posts.route');

module.exports = function loadroutes () {
    const app = express(); 
    app.use(cors());
    
    app.engine('handlebars', exphbs());
    app.set('view engine', 'handlebars');
    app.use(express.static('public'));
    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));

    app.use('/user', user); 
    app.use('/order', order);
    app.use('/support', support);
    app.use('/countries', countries);    
    app.use('/posts', posts);    

    if(DEV) {
        app.get('/' , async (_, res) => {
            try {
                const user = {
                    email : 'yassine.benamar.1@esprit.tn',
                    generateVerificationToken : () => "verif"
                };
                await Mailer.sendConfirmationMail(user, res);
                res.send();
            } catch(e) {
                console.log(e)
                res.sendStatus(500)
            }
        })
    }
    return app
}