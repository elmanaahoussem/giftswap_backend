const { Router } = require("express");
const AuthGuard = require("../classes/AuthGuard");
const { PostsCotroller } = require("../controllers/posts.controller");

const router = Router();

router.get('', PostsCotroller.getAll)
router.post('/create', AuthGuard, PostsCotroller.createPost)
router.post('/:id/edit', AuthGuard, PostsCotroller.updatePost)
router.get('/:id', PostsCotroller.getPost)
router.delete('/:id', AuthGuard, PostsCotroller.deletePost)

exports.posts = router;