const express = require('express');
const router = express.Router();

 
const countries_controller = require('../controllers/countries.controller');


router.get('', countries_controller.getAll); 


module.exports = router;



