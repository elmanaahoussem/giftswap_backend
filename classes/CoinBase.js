const axios = require("axios")

const GetRate = async (src, dest) => {
    try {
       const { data : res } = await axios.get("https://api.coinbase.com/v2/exchange-rates?currency=" + src);
       return res.data.rates[dest];
    } catch(e){
        throw e;
    }
}
const CoinBase = {
    GetRate
}

exports.CoinBase = CoinBase;