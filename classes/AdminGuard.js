const Users = require('../models/user.model');  
const { USER_VERIFICATION_TOKEN_SECRET } = require('./Env');
const jwt = require('jsonwebtoken')

const AdminGuard = async (req, res, next) => {

    try{
        let [_, token] = req.headers.authorization.split(' ');
        let payload = jwt.verify(
            token,
            USER_VERIFICATION_TOKEN_SECRET
        );
        let user = await Users.findById(payload.id);
        if(user.type != 'admin')
            throw new Error('Not an Admin');
        req.user = user.toJSON();
        next();   
    } catch(e) {
        res.status(401).send({ message : "Access denied" });
    } 

}

module.exports = AdminGuard;