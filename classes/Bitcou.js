const axios = require("axios");
const { GRAPHQL_KEY, GRAPHQL_URL } = require("./Env");
const { v4 : uuidv4 } = require('uuid')


let headers = {
    'x-api-key' : GRAPHQL_KEY
}

const GetProductById = async (id) => {
    const query = `{
        products(filter : {id: ${id}}) {
          id,
          fullName,  
          fixedPrice,
          isVariablePrice,
          minVariablePrice,
          maxVariablePrice,
          eurFixedPrice,
          eurMinVariablePrice,
          eurMaxVariablePrice,
          currency,
          urlImage,
          requireMail,
          requirePhone,
          requireOther
        }
      }`;

    try {
        const { data : response } = await axios.post(GRAPHQL_URL, { query }, { headers });
        if(response.data.products.length > 0)
            return response.data.products[0];
        throw new Error("Product not found");
    } catch(e){
        console.log(e.response.data || e);
    }

}

const SavePurshase = async (orderItem) => {
    const transactionId = uuidv4();
    const purchase = {
        transactionID : transactionId,
        userInfo : {
            "email": orderItem.email,
            "name": orderItem.name,
            "country": orderItem.country.code,
            "phoneCountryCode": orderItem.country.phonePrefix,
            "phoneNumber": orderItem.phone,
            "serviceNumber": orderItem.serviceNumber
        },
        productID: orderItem.productId,
        totalValue: orderItem.price
    }
    if(!orderItem.phoneNumber){
        delete orderItem.phoneCountryCode;
        delete orderItem.phoneNumber;
    }
    if(!orderItem.serviceNumber) delete purchase.userInfo.serviceNumber ;
    const query = `mutation createOrder($purchase: PurchaseInput!) {
            createPurchase(purchase: $purchase) {
                id
                redeemCode
                product {
                    fullName
                }
                EndUserEmail
                totalValue
                transactionID
                receipt
            }
    }`
    console.log({ purchase });
    const { data : result } = await axios.post(GRAPHQL_URL, { query, variables : { purchase } }, { headers });
    
    if(result.errors && result.errors.length > 0 ){
        result.errors.map((e) => console.log("Error : ", e))
        const err = new Error();
        err.response = {data : result.errors};
        throw err;
    }
    return result.data.createPurchase;
}

const Bitcou = {
    GetProductById,
    SavePurshase
}

exports.Bitcou = Bitcou;