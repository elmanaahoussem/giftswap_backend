const { GIFTSWAP_APIURL, GMAIL_PASSWORD, GMAIL_USERNAME, DEV, GIFTSWAP_URL } = require('./Env');
const moment = require('moment')
const nodemailer = require('nodemailer');
const { TicketTypes } = require('../models/ticket.model');

const transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: GMAIL_USERNAME,
        pass: GMAIL_PASSWORD,
    },
} , {
    from : "Giftswap no-reply@giftswap.com"
});

async function sendPasswordResetEmail(user, url, res){
    const html = await asyncRender(res, 'reset-password', { url, GIFTSWAP_APIURL, DEV });
    await transporter.sendMail({
        to: user.email,
        subject: 'Reset password request',
        html
    })
}


async function sendConfirmationMail(user, res) {
    const verificationToken = user.generateVerificationToken()
    const link = `${GIFTSWAP_APIURL}/user/verify/${verificationToken}`;
    if(DEV) console.log(link);
    const html = await asyncRender(res, 'register-email', { url: link, GIFTSWAP_APIURL })
    await transporter.sendMail({
        to: user.email,
        subject: 'Verify Account',
        html
    })
}

async function sendTicketCreated(ticket, user, res) {
    const link = `${GIFTSWAP_URL}/support/mytickets/${ticket.id}`;
    const ticketType = TicketTypes[ticket.type];
    const ticketDate = moment(ticket.date).format('MMMM dd, YYYY');
    const html = await asyncRender(res, 'ticket-created', { url: link, ticket, ticketType, ticketDate })
    await transporter.sendMail({
        to: user.email,
        subject: 'Ticket created',
        html
    })
}

async function sendTicketUpdated(ticket, user, res) {
    const link = `${GIFTSWAP_URL}/support/mytickets/${ticket.id}`;
    const html = await asyncRender(res, 'ticket-updated', { url: link, ticketId : ticket.id });
    await transporter.sendMail({
        to: user.email,
        subject: 'Ticket created',
        html
    })
}

async function sendPasswordChangedEmail(user, res){
    const html = await asyncRender(res, 'password-changed', { url : GIFTSWAP_URL, GIFTSWAP_APIURL, DEV });
    await transporter.sendMail({
        to: user.email,
        subject: 'Password Changed',
        html
    })
}

async function sendUserConfirmed(user, res){
    const html = await asyncRender(res, 'user-confirmed');
    await transporter.sendMail({
        to: user.email,
        subject: 'Your account is now verified',
        html
    })
}

const asyncRender = (res, view, args) => {
    return new Promise((resolve, reject) => {
        res.render(view, { layout : null, ...args }, (err, html) => {
            if(err) reject(err);
            resolve(html);
        })
    })
}

exports.Mailer = {
    sendPasswordResetEmail,
    sendPasswordChangedEmail,
    sendConfirmationMail,
    sendTicketCreated,
    sendUserConfirmed,
    sendTicketUpdated
}