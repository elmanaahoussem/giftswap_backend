const { v4 : uuidv4 } = require("uuid");
const fs = require('fs')
const path = require('path');

function saveFile(file, destination) {
    const oldpath = file.filepath;
    const ext = file.originalFilename.slice(file.originalFilename.lastIndexOf('.') + 1);
    const unique_name = `${uuidv4()}.${ext}`;
    const newpath = `/public/uploads/${destination}/${unique_name}`;
    if (!fs.existsSync(path.join(path.resolve(), '/public/uploads/', destination))){
        fs.mkdirSync(path.join(path.resolve(), '/public/uploads/', destination), { recursive : true });
    }
    fs.renameSync(oldpath, path.join(path.resolve(), newpath));
    return `/uploads/${destination}/${unique_name}`;
}

exports.Files = {
    saveFile
}