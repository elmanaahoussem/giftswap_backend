const axios = require("axios");
const { Countries } = require('../models/country.model');

const GetCountries = () => {
    axios.get("https://raw.githubusercontent.com/mledoze/countries/master/dist/countries-unescaped.json")
        .then(async ({ data }) => {
            await Countries.deleteMany();
            const countries = [];
            for(let country of data){
                const { name, callingCodes, cca2 } = country;
                if(callingCodes.length == 0 || callingCodes[0].trim() == "") continue;
                countries.push({ 
                    name : name.common,
                    phonePrefix : callingCodes[0],
                    code : cca2
                })
            };
            Countries.insertMany(countries)
                .catch(console.log);
        })
}

exports.CountriesAPI = {
    GetCountries
}