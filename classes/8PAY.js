const axios = require("axios");
const { DEV, EIGHTPAY_ENDPOINT_SHORTLINK, EIGHTPAY_KEY, GIFTSWAP_APIURL, GIFTSWAP_URL, METAMASK_WALLET } = require("./Env");


const config = {
    headers: { Authorization: `Bearer ${EIGHTPAY_KEY}` }
};

const GetLink = (orderId, total, token) => {
    let bodyParameters = {
        name: "GiftSwap Payment",
        params: {
            "description": "GiftSwap Payment",
            "receivers": [
                METAMASK_WALLET
            ],
            "amounts": [total],
            "token": token.toUpperCase(),
            "category": "Shop",
            "callbackSuccess": `${GIFTSWAP_APIURL}/order/${orderId}/processing`,
            "callbackError": `${GIFTSWAP_URL}/order/${orderId}/failed`,
            "webhook": DEV ? '' : `${GIFTSWAP_APIURL}/order/${orderId}/completed`,
            "extra" : { orderId }
        }
    }
    return axios.post(`${EIGHTPAY_ENDPOINT_SHORTLINK}/one-time/short-urls`, bodyParameters, config);
}

// Verify payment is comping from 8pay (found in 8pay docs)
const VerifyIntegrity = (notification) => {
    return axios.get(`${EIGHTPAY_ENDPOINT_SHORTLINK}/webhook-notifications/${notification.id}`, config);
}

const EIGHTPAY = {
    GetLink,
    VerifyIntegrity
}

module.exports = EIGHTPAY;