const Errors = {
    PRICE_CONVERSION : "PRICE_CONVERSION",
    EIGHTPAY_SHORTLINK : "EIGHTPAY_SHORTLINK"
}

module.exports = Errors;