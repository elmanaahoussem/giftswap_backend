const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CountrySchema = new Schema({
    name : {type: String, required : true},
    code : { type : String, required : true },
    phonePrefix : { type : String, required : true }
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    }
});
 
const Countries = mongoose.model('Country', CountrySchema);

module.exports = {
    Countries,
    CountrySchema
}