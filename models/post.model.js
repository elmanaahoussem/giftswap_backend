const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PostSchema = new Schema({
    title : {type: String, required : true},
    content : { type : String, required : true },
    author : { type : String },
    author_pic : { type : String },
    picture : { type : String, required : true },
    date : { type : Date, required : true },
    featured : { type : Boolean }
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    }
});
 
const Posts = mongoose.model('Post', PostSchema);

module.exports = {
    Posts
}