const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PurchaseSchema = new Schema({ 
    productId : { type: Number , required: true },
    redeemCode: { type: String, required: true },
    transactionId: { type: String, required : true }
});

let OrderItemSchema = new Schema({ 
    productId : { type: Number , required: true }, 
    variantId : { type :  String, requried : true },
    name : { type: String , required: true }, 
    currency : { type: String , required: true }, 
    image : { type: String },
    quantity : { type: Number , required: true },
    price : { type: Number , required: true },
    purchases : [PurchaseSchema],
    serviceNumber : { type: String }
});

module.exports = OrderItemSchema;