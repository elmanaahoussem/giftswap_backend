const bcrypt = require("bcryptjs");
const { isEmail } = require("validator");
const jwt = require('jsonwebtoken');
const { CountrySchema } = require("./country.model");

var mongoose = require("mongoose"),
  Schema = mongoose.Schema,
  SALT_WORK_FACTOR = 10;

var UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    validate: [isEmail, "invalid email"],
    createIndexes: { unique: true },
  },
  fullname : String,
  country : CountrySchema,
  password: { type: String, required: true },
  phone: {
    type: String,
    // validate: [isMobilePhone, "invalid Phone Number"],
    // required: true,
  },
  type: { type: String, default: 'user' },
  isActive: { type: Boolean, default: false },
}, {
  toJSON: {
    transform(_, ret) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.__v;
      delete ret.type;
      delete ret.password;
      delete ret.salt;
    }
  }
});

UserSchema.pre("save", async function save(next) {
  if (!this.isModified("password")) return next();
  try {
    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    this.password = await bcrypt.hash(this.password, salt);
    return next();
  } catch (err) {
    return next(err);
  }
});

UserSchema.methods.validatePassword = async function validatePassword(data) {

  return bcrypt.compare(data, this.password);
};

UserSchema.methods.generateVerificationToken = function () {
  const user = this;
  const verificationToken = jwt.sign(
    { ID: user._id },
    process.env.USER_VERIFICATION_TOKEN_SECRET,
    { expiresIn: "7d" }
  );
  return verificationToken;
};

module.exports = mongoose.model("User", UserSchema);
