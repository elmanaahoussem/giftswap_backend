const { model, Schema, Types } = require("mongoose");

const RefreshTokenSchema = new Schema({
    user : { type: Types.ObjectId, ref: 'User' },
    connectionId : String,
    token : String,
    expiration : Date
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    },
    timestamps : true
})

const RefreshToken = model('RefreshToken', RefreshTokenSchema);

module.exports = RefreshToken;