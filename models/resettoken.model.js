const { model, Schema, Types } = require("mongoose");

const ResetTokenSchema = new Schema({
    user : { type: Types.ObjectId, ref: 'User' },
    token : String,
    expiration : Date
}, {
    toJSON : {
        transform(_, ret){
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
        }
    },
    timestamps : true
})

const ResetToken = model('ResetToken', ResetTokenSchema);

module.exports = ResetToken;