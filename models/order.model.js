const mongoose = require('mongoose');
const { stringify } = require('uuid');
const { CountrySchema } = require('./country.model');
const OrderItemSchema = require('./orderitem.model');
const Schema = mongoose.Schema;

let OrderSchema = new Schema({ 
    items : {type: [OrderItemSchema] , required: true },
    token : { type: String , required: true },
    label : { type: String },
    amount : { type: Number , required: true },
    amountUSD : { type: Number , required: true },
    currency : { type: String , required: true },
    status : { type : String, enum: ['WAITING', 'CANCELED', 'DELIVERED', 'PROCESSING'], default : 'WAITING', required : true },
    note : { type : String, required : false },
    email : { type : String, required : false },
    phone : { type : String, required : false },
    country : { type : CountrySchema, required : false },
    phonePrefix : { type : String, required : false },
    user : { type : mongoose.Schema.Types.ObjectId, ref:'User' , required: true },
    transactionHash : { type: String },
    date : { type: Date , required: true } ,  
}, {
    toJSON : {
        transform(_, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret._v;
          }
    }
});

const OrderStatus = {
    WAITING : 'WAITING',
    DELIVERED : 'DELIVERED',
    PROCESSING : 'PROCESSING',
    CANCELED : 'CANCELED'
}

const Order = mongoose.model('Order', OrderSchema)

module.exports = {
    OrderStatus,
    Order
};