const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Message = new Schema({
    message: {type: String, required: true},
    ticket : {type : mongoose.Schema.Types.ObjectId, ref:'Ticket',required : true}, 
    user : { type : mongoose.Schema.Types.ObjectId, ref:'User' , required : true},   
    sentByAdmin : Boolean,   
    date : {type: Date}
});
 
module.exports = mongoose.model('Message', Message);