const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Ticket = new Schema({
    title: { type: String, required: true },
    type: { type: String, required: true },
    brand : { type: String },
    order : { type: mongoose.Schema.Types.ObjectId, ref: 'Order', required: false },
    productId : { type: mongoose.Schema.Types.ObjectId, required: false },
    content : { type: String },
    messages: [
        {   
            type: mongoose.Schema.Types.ObjectId, ref: 'Message', required: true 
        }
    ],
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    state: { 
        type: String,
        enum : ["WAITING", "ADMIN_ANSWERED", "USER_ANSWERED", "SOLVED", "CLOSED"],
        default : "WAITING"
    },
    date: { type: Date }
}, {
    toJSON: {
      transform(_, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      }
    }
  });

const TicketTypes = {
    PurchaseProblem: 'Problem purchasing product(s)',
    UndeliveredProduct: 'Product(s) not delivered',
    PaymentProblem: 'Problem with Payment',
    Other: 'Other'
}
module.exports = mongoose.model('Ticket', Ticket);

module.exports.TicketTypes = TicketTypes