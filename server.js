require('dotenv').config()

const loadroutes = require('./routes/routes.loader');  
const app = loadroutes()  
var geoip = require('geoip-country');
const appPort = process.env.PORT || 8080 
const path = require("path");
// Set up mongoose connection
const mongoose = require('mongoose');
const { CountriesAPI } = require('./classes/CountriesAPI');

mongoose.connect(process.env.MONGODB,{useNewUrlParser: true,useUnifiedTopology: true})   
    .then(() => {
        console.log('Conntected To Mongodb')
        
        CountriesAPI.GetCountries();
        
        app.listen(appPort, async () => {
            console.log('Server is up and running on port ' + appPort);  
        
        });
    })   
    .catch(err => console.error('Connection failed...'));

 


