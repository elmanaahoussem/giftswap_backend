const User = require('../models/user.model');
const ResetToken = require('../models/resettoken.model');
const { GIFTSWAP_URL, USER_VERIFICATION_TOKEN_SECRET } = require('../classes/Env');
const RefreshToken = require('../models/refreshtoken.model');
const { Mailer } = require('../classes/Mailer');

const jwt = require('jsonwebtoken');
const Crypto = require('crypto')
const moment = require('moment')
const bcrypt = require('bcrypt')


/* URL : '/add' */
exports.add_user = function (req, res, next) {
    let user = new User(
        {
            email: req.body.email,
            password: req.body.password,
            phone: req.body.phone,
            isActive: false,
            type: 'user'
        }
    );

    User.findOne({ email: req.body.email }, async function (err, docs) {
        if (docs) {
            res.status(400).send("email already exists")
        } else {
            try{
                await user.save();
                await Mailer.sendConfirmationMail(user, res)
                return res.status(201).send({
                    message: `Sent a verification email to ${req.body.email}`
                });

            } catch {
                return next(err.message);
            }
        }
    })

};

exports.add_admin = function (req, res, next) {
    let user = new User(
        {
            email: req.body.email,
            password: req.body.password,
            fullname : req.body.fullname,
            phone: req.body.phone,
            isActive: true,
            type: 'admin'
        }
    );

    User.findOne({ email: req.body.email }, function (err, docs) {
        if (docs) {
            res.status(400).send("email already exists")
        } else {
            user.save(async function (err) {
                if (err) {
                    return next(err.message);
                }
                return res.status(201).send();
            })
        }
    })

};

/* URL : 'verify/:token' */
exports.verify_user = async (req, res) => {
    const { token } = req.params    // Check we have an id

    if (!token) {
        return res.status(422).send({
            message: "Missing Token"
        });
    }    // Step 1 -  Verify the token from the URL
    let payload = null
    try {
        payload = jwt.verify(
            token,
            USER_VERIFICATION_TOKEN_SECRET
        );
    } catch (err) {
        console.log(err)
        return res.status(500).send(err);
    }

    try {
        // Step 2 - Find user with matching ID
        const user = await User.findOne({ _id: payload.ID }).exec();
        if (!user) {
            return res.status(404).send({
                message: "User does not  exists"
            });
        }        // Step 3 - Update user verification status to true
        user.isActive = true;
        Mailer.sendUserConfirmed(user, res);
        await user.save(); return res.redirect(`${GIFTSWAP_URL}/account/verified`);
    } catch (err) {
        return res.status(500).send(err);
    }
}

/* URL : '/:id' */
exports.get_user = function (req, res, next) {
    // fetch the user and test password verification 
    User.findById({ _id : req.params.id}, async function (err, user) {
        if (!user)
            res.sendStatus(403)
        if (err) throw err;
        return res.send(user);
    });
};

/* URL : '/:id' */
exports.get_admin = function (req, res, next) {
    // fetch the user and test password verification 
    User.findOne({ email: req.query.email, type: 'admin' }, async function (err, user) {
        if (!user)
            return res.sendStatus(403)
        if (err) throw err;
        return await user.validatePassword(req.query.password) ? res.send(user) : res.sendStatus(403);
    });
};

exports.resend_confirmation = (req, res) => {
    User.findOne({ email : req.body.email })
        .then(async (user) => {
            if(!user)
                return res.status(400).send({ message : "User not found" });
            await Mailer.sendConfirmationMail(user, res);
            res.send({ message : "Confirmation email sent successfully" });
        })
        .catch(() => { return res.status(500).send({ message : "An error occured" }); })
}


exports.login = function (req, res, next) {
    User.findOne({ email: req.body.email }, async function (err, user) {
        if (err) throw err;
        if (!user)
            return res.sendStatus(403)
        if (!await user.validatePassword(req.body.password))
            return res.status(403).send({ message : "BadCredentials" });

        if(!user.isActive){
            return res.status(403).send({ message : "AccountInactive" });
        }
        const refreshToken = await RefreshToken.create({
            user: user.id,
            token: randomString(),
            expiration: moment().add(7, 'days').toDate(),
            connectionId: req.body.connectionId
        });


        const token = jwt.sign(
            { id: user._id },
            process.env.USER_VERIFICATION_TOKEN_SECRET,
            { expiresIn: "1d" }
        )
        res.send({ message: "User logged in", user: { ...user.toJSON(), token, refreshToken: refreshToken.token } });
    });
};

/* URL : '/:id' */
exports.get_all_users = function (req, res, next) {
    // fetch the user and test password verification 
    User.find({ type : 'user' }, async function (err, user) {
        if (!user)
            res.sendStatus(403)
        if (err) throw err;
        return res.send(user)
    });
};

exports.get_all_admin = function (req, res, next) {
    // fetch the user and test password verification 
    User.find({ type : 'admin' }, async function (err, user) {
        if (!user)
            res.sendStatus(403)
        if (err) throw err;
        return res.send(user)
    });
};

/* URL : '/:id/update' */
exports.update_user = function (req, res, next) {
    User.updateOne({ _id: req.params.id }, { $set: req.body }, function (err, Website) {
        if (err) return next(err);
        res.send(Website);
    });
};

/* URL : '/:id/delete' */
exports.delete_user = function (req, res, next) {
    User.deleteOne({ _id: req.params.id }, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};

exports.verify_email = (req, res) => {
    res.render('register-email', { url: "http://www.google.com" })
}

exports.refresh = async (req, res, next) => {
    const { token, refreshToken, connectionId } = req.body;
    const privateKey = process.env.USER_VERIFICATION_TOKEN_SECRET;
    const payload = jwt.decode(token);
    const old = await RefreshToken.findOne({ connectionId, token: refreshToken, user: payload.id });
    if (old && old.expiration > new Date()) {
        const user = await User.findById(payload.id);
        const token = jwt.sign({ id: user.id }, privateKey, { expiresIn: "1d" });
        old.expiration = moment(old.expiration).add(1, 'days').toDate();
        old.save();
        const expiresIn = moment().add(1, "days").toDate();
        return res.json({ id: user.id, token, expiresIn });
    } else {
        res.status(400).send({ message: 'Refresh token expired, you have to login again' })
    }
}

exports.request_password_reset = async (req, res) => {

    const user = await User.findOne({ email: req.body.email });

    if (!user) return res.status(400).send({ message: "User does not exist" });
    let token = await ResetToken.findOne({ user: user._id });
    if (token) await token.deleteOne();
    let resetToken = Crypto.randomBytes(32).toString("hex");
    const hash = await bcrypt.hash(resetToken, 10);
    await new ResetToken({
        user: user._id,
        token: hash,
        createdAt: Date.now(),
    }).save();

    const link = `${GIFTSWAP_URL}/auth/reset-password?token=${resetToken}&id=${user._id}`;
    try {
        await Mailer.sendPasswordResetEmail(user, link, res);
        res.send({ message: "Password request email sent" })
    } catch {
        return res.status(500).send({ message : "An Error Occured" });
    }
}

exports.reset_password = async (req, res) => {

    const { id, token, password } = req.body;
    let passwordResetToken = await ResetToken.findOne({ user : id });
    if (!passwordResetToken) {
        throw new Error("Invalid or expired password reset token");
    }
    const isValid = await bcrypt.compare(token, passwordResetToken.token);
    if (!isValid) {
        throw new Error("Invalid or expired password reset token");
    }
    const hash = await bcrypt.hash(password, 10);
    try {
        await User.updateOne(
            { _id: id },
            { $set: { password: hash } },
            { new: true }
        );
        const user = await User.findById({ _id: id });
        await Mailer.sendPasswordChangedEmail(user, res);
        await passwordResetToken.deleteOne();
        res.send({ message : "Password changed successfully" });
    } catch (e) {
        res.status(500).send({ messagee : "An error occured" });
    }
}

function randomString(size = 21) {
    return Crypto
        .randomBytes(size)
        .toString('base64')
        .slice(0, size)
}