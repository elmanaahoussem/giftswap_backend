const { GIFTSWAP_URL, DEV, GIFTSWAP_BENEFIT } = require('../classes/Env');
const Errors = require('../classes/Errors');
const { Bitcou } = require('../classes/Bitcou');
const { OrderStatus, Order } = require('../models/order.model');
const EIGHTPAY = require('../classes/8PAY');
const { CoinBase } = require('../classes/CoinBase');
const userModel = require('../models/user.model');

exports.redeem = async (req, res) => {
    const { id } = req.params;
    const { product: productId } = req.query;
    const order = await Order.findById(id)
        .populate('user');
    if (!order)
        return res.status(400).send({ message: "Order not found" });

    const orderItem = order.items.find(p => p.productId == productId);
    try {
        orderItem.email = order.email;
        orderItem.phone = order.phone;
        orderItem.country = order.country;
        orderItem.phonePrefix = order.phonePrefix;
        await GetOrderItemPurchases(orderItem);
        const gotAllPurchases = order.items.every(oi => oi.purchases.length == oi.quantity);
        if (gotAllPurchases)
            order.status = OrderStatus.DELIVERED;
        order.save();
        res.send({ message: "Product redeem code successfully acquired", purchases: orderItem.purchases });
    } catch (err) {
        order.save();
        console.log(err.message)
        res.status(400).send({ message: "Could not get product redeem code", errors: err.response.data });
    }
}

exports.processing = async (req, res) => {
    const order = await Order.findById(req.params.id);
    order.status = OrderStatus.PROCESSING;
    await order.save();
    res.redirect(`${GIFTSWAP_URL}/orders/${req.params.id}/completed`);
}

exports.pay = async (req, res) => {
    const order = await Order.findById(req.params.id);
    if (!order)
        return res.status(400).send({ message: "Order not found" });
        
    const usdRate = await CoinBase.GetRate(order.token, 'USD');
    const total = order.amountUSD / usdRate;

    EIGHTPAY.GetLink(order.id, total, order.token)
        .then(({ data }) => res.send(data.link))
        .catch(() => {
            console.log(err.response ? err.response.data : err);
            res.status(400).send({ error: Errors.EIGHTPAY_SHORTLINK, message: "order failed" });
        })
}

exports.checkout = async (req, res) => {

    let totalCurrency = 0, totalUSD = 0, total = 0; i = 0;
    let label = '';
    let currency = '';
    try {
        const products = req.body.products;
        const usdRate = await CoinBase.GetRate(req.body.token, 'USD');
        for (const p of products) {
            const product = await Bitcou.GetProductById(p.productId);
            const rate = await CoinBase.GetRate(req.body.token, p.currency);
            console.log({rate});
            validateOrderItem(product, p, req.body);
            p.name = product.fullName;
            p.image = product.urlImage;
            p.price = p.price;
            p.email = req.body.email;
            p.currency = product.currency;
            currency = product.currency;
            label += (i > 0 ? ', ' : '') + p.name;
            totalUSD += await getUSDPrice(product, p.total);
            totalCurrency += p.total / rate;
            i++;
        }
        total = totalUSD / usdRate;
    } catch (err) {
        console.log(err);
        return res.status(400).send({ error: Errors.PRICE_CONVERSION, message: "Failed to convert token price" });
    }

    const order = new Order({
        items: req.body.products,
        label,
        user: req.user.id,
        token: req.body.token,
        amount: totalCurrency,
        amountUSD: totalUSD,
        currency,
        paid: false,
        country: req.body.country,
        phone: req.body.phone,
        email: req.body.email,
        note: req.body.note,
        date: new Date(),
    });
    await order.save();

    let user = await userModel.findById(req.user.id);
    user.phone = req.body.phone;
    user.country = req.body.country;
    user.save();
    EIGHTPAY.GetLink(order.id, total, req.body.token)
        .then(({ data }) => {
            res.send(data.link)
        })
        .catch((err) => {
            
            console.log(err.response ? err.response.data : err);
            res.status(400).send({ error: Errors.EIGHTPAY_SHORTLINK, message: "order failed" });
        })
}

exports.cancel = async function (req, res) {
    try {
        Order.findById(req.params.id)
            .then((order) => {
                order.status = OrderStatus.CANCELED;
                order.save();
            })
        res.send({ message: "Order paid successfully" });
    } catch (e) {
        res.status(400).send({ message: "Order payment not found" })
    };
}

// API Called by 8PAY after a payment is maid
exports.completed = async function (req, res) {
    const { id } = req.params;
    const { transactionHash } = req.body;
    try {
        !DEV && await EIGHTPAY.VerifyIntegrity(req.body);
        const order = await Order.findById(id)
            .populate('user');
        if (order.status == OrderStatus.DELIVERED) {
            return res.send({ message: "Order treated already" });
        }
        order.transactionHash = transactionHash;
        await order.save();
        let purchaseSuccess = true;
        // Save purchases to Bicou and get redeem codes
        for (let orderItem of order.items) {
            try {
                orderItem.phone = order.phone;
                orderItem.country = order.country;
                orderItem.email = order.email;
                await GetOrderItemPurchases(orderItem);
            } catch (purchaseError) {
                purchaseSuccess = false;
            }
        }
        const gotAllPurchases = order.items.every(oi => oi.purchases.length == oi.quantity);
        if (gotAllPurchases)
            order.status = OrderStatus.DELIVERED;

        // If all purchases are saved succesfully change order to 'Delivered'
        if (purchaseSuccess) {
            order.status = OrderStatus.DELIVERED;
            order.save();
        } else {
            order.save();
            throw new Error("Failed to purchase some or all order items")
        }
        res.send({ message: "Order paid successfully" });
    } catch (err) {
        console.error(err);
        res.status(400).send();
    }
}

exports.myorders = async function (req, res) {
    const { page = 1 } = req.query;
    try {
        let query = Order.find({
            user: req.user.id
        }).sort([['date', -1]])

        if(page != -1){
            query = query.skip((page - 1) * 10)
            .limit(10);
        }
        const orders = await query;
        res.send(orders);
    } catch (err) {
        console.log(err)
        res.status(400).send();
    }
}

exports.add = function (req, res, next) {
    let orderInstance = new Order(
        {
            items: req.body.items,
            user: req.user.id,
            token: req.body.token,
            amount: req.body.amount,
            date: new Date(),
        }
    );
    orderInstance.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Order Created successfully')
    })
};

exports.get_all_orders = async function (req, res) {

    // fetch the user and test password verification 
    const { page = 1, items = 10 } = req.query;
    try {
        const orders = await Order.find()
            .sort([['date', -1]])
            .skip((page - 1) * items)
            .limit(items);
        const count = await Order.countDocuments();
        res.send({ items : orders, count })
    } catch(e){
        console.log(e);
        res.status(500).send({ message : "an error occured" })
    }


};

exports.get_order = async function (req, res) {
    Order.findById(req.params.id, async function (err, order) {
        if (err) throw err;
        if (!order || (req.user.type != 'admin' && order.user.toString() != req.user.id.toString()))
            return res.sendStatus(403)
        return res.send(order)
    });

};

exports.get_user_orders = async function (req, res) {
    const { page = 1, items = 10 } = req.query;
    Order.findBy({ user: req.params.id }, async function (err, orders) {
        if (err) throw err;
        return res.send(orders)
    })
        .sort([['date', -1]])
        .skip((page - 1) * items)
        .limit(items);

};

const GetOrderItemPurchases = async (orderItem) => {
    orderItem.purchases = orderItem.purchases || [];
    const purchasesCount = orderItem.quantity - orderItem.purchases.length;

    for (let i = 0; i < purchasesCount; i++) {
        const purchase = await Bitcou.SavePurshase(orderItem);
        orderItem.purchases.push({
            productId: orderItem.productId,
            transactionId: purchase.transactionID
        })
    }
}

const validateOrderItem = (product, orderItem, order) => {
    let { requiredEmail, requiredPhone, requiredOther, isVariablePrice, maxVariablePrice, minVariablePrice } = product;
    if (requiredOther && !product.serviceNumber)
        throw new Error("Service Number required");
    if (requiredEmail && !order.email)
        throw new Error("Email required");
    if (requiredPhone && !order.phone)
        throw new Error("Phone is required");

    if (isVariablePrice && (orderItem.price < minVariablePrice || orderItem.price > maxVariablePrice))
        throw new Error("Invalid Price");

    if (!isVariablePrice && orderItem.price != product.fixedPrice)
        throw new Error("Invalid Price");
}

const getUSDPrice = async (product, price) => {
    if (product.currency == 'USD') return price;
    if (product.isVariablePrice) {
        const CURRENCY_TO_USD = await CoinBase.GetRate(product.currency, 'USD');

        return price * CURRENCY_TO_USD;
    }
    const EUR_TO_USD = await CoinBase.GetRate('EUR', 'USD');
    const { eurFixedPrice } = product;
    return eurFixedPrice * EUR_TO_USD * GIFTSWAP_BENEFIT;
}