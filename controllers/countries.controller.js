const { Countries } = require('../models/country.model');

exports.getAll = (_, res) => {
    Countries.find()
        .sort({ name : 1 })
        .then((countries) => {
            res.send(countries)
        })
        .catch(() => res.status(500).send());
} 