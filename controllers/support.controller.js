const Ticket = require('../models/ticket.model');
const Message = require('../models/message.model');
const nodemailer = require('nodemailer');
const { Types } = require('mongoose');
const { Mailer } = require('../classes/Mailer');

exports.create = async function (req, res,next) {
    try {
        let ticketInstance = new Ticket(
            { 
                ...req.body,
                user :  req.user.id , 
                solved : false,
                date : new Date()
            }
        );
        await ticketInstance.save();
        Mailer.sendTicketCreated(ticketInstance.toJSON(),req.user, res);
        return res.status(200).send({ message: `Ticket Submitted` });
    } catch(e){
        console.log(e);
        return res.status(500).send({ message : 'An error occured' });
    }
 
}; 

exports.get_user_tickets = (req, res) => {
    const { page } = req.query
    Ticket.find({ user : req.user.id })
        .sort({ date : -1 })
        .skip((page-1)*10)
        .limit(10)
        .then((tickets) => { res.send(tickets) })
        .catch(() => res.status(500).send("An Error occured"));
}

exports.reply = async function (req,res,next) {
    const { id } = req.params;
        const ticket = await Ticket.findById(id)
            .populate('user');
    if(!ticket)
        return res.status(400).send({ message : "Ticket not found" })

    if(req.user.type == 'admin')
        ticket.state = "ADMIN_ANSWERED";
    else ticket.state = "USER_ANSWERED";
    await ticket.save();
    let messageInstance = new Message(
        { 
            message : req.body.message,
            ticket :  id,
            user :  req.user.id, 
            sentByAdmin : req.user.type == 'admin',
            date : new Date()
        }
    ); 
    messageInstance.save(function (err) {
        console.log(err);
        if(req.user.type == "admin"){
            console.log()
            Mailer.sendTicketUpdated(ticket, ticket.user, res);
        }
        return res.status(200).send({ message: `Message Submitted` });
    })
}


exports.contact = async function (req, res,next) {
     

    const transporter = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user: process.env.GMAIL_USERNAME,
            pass: process.env.GMAIL_PASSWORD,
        },
    }); 

    transporter.sendMail({
        to: "hello@elmanaa.wtf",
        subject: req.body.subject,
        html: req.body.message
      })
      return res.status(200).send({ message: `Submitted` });
 
 
}; 


/* URL : '/:id' */
exports.get_all_tickets = async function (req, res) {
    // fetch the user and test password verification 
    const { page = 1, items = 10, user, type, state } = req.query;
    try {
        const filter = { user, type, state };
        if(user) filter.user = user.padEnd(24, '0');
        for(let prop in filter){
            if(!filter[prop])
                delete filter[prop];
        }
        const tickets = await Ticket.find(filter)
            .sort([['date', -1]])
            .skip((page - 1) * items)
            .limit(items);
        const count = await Ticket.countDocuments(filter);
        res.send({ items : tickets, count })
    } catch(e){
        console.log(e);
        res.status(500).send({ message : "an error occured" })
    }
};
/* URL : '/:id' */
exports.get_ticket = async function (req, res) {    
    try {
        const ticket = await Ticket.findById(req.params.id)
            .populate('user')
            .populate('order');

        let messages = [];
         
        if (!ticket)
            res.status(400).send({ message : "Ticket not found" });
        
        if(ticket.user.id != req.user.id && req.user.type != 'admin')
            return res.status(403).send({ message : "Access unauthorized" });
        
        messages = await Message.find({ ticket : req.params.id })
            .populate('user');
        ticket.messages = messages;
        
        return res.send(ticket);   
    } catch (e){
        console.log(e);
        return res.status(500).send({ message : "An error occured" });
    } 
};

exports.solveTicket = async function (req, res,next) {
    
    const ticket = await Ticket.findById(req.params.id);

    if (!ticket)
        res.status(400).send({ message : "Ticket not found" });
    
    if(ticket.user.toString() != req.user.id && req.user.type != 'admin')
        return res.status(403).send({ message : "Access unauthorized" });
    
    Ticket.updateOne({ _id: req.params.id}, {state: "SOLVED"}, function (err, ticket) {
        if (err) return next(err);
        res.send(ticket);
    }); 
}
 
exports.unsolvedtickets = async function (req, res,next) {
    
    if ( req.query.type )
    Ticket.find({solved:false,type:req.query.type}, function (err, tickets) {
        if (err) return next(err);
        res.send(tickets);
    })
    else
    Ticket.find({solved:false}, function (err, tickets) {
        if (err) return next(err);
        res.send(tickets);
    })
 
}; 

exports.solvedtickets = async function (req, res,next) {
 
    if ( req.query.type )
    Ticket.find({solved:true,type:req.query.type }, function (err, tickets) {
        if (err) return next(err);
        res.send(tickets);
    }) 
    else
    Ticket.find({solved:true}, function (err, tickets) {
        if (err) return next(err);
        res.send(tickets);
    }) 
}; 

exports.update_article = function (req, res,next) {
    Ticket.updateOne({ _id: req.params.id}, {$set: req.body}, function (err, ticket) {
        if (err) return next(err);
        res.send(ticket);
    });
};

exports.delete_ticket = function (req, res,next) {
    Ticket.deleteOne({ _id: req.params.id}, function (err) {
        if (err) return next(err);
        res.status(200);
    })
};