const formidable = require('formidable');
const { Files } = require('../classes/Files');
const { Posts } = require('../models/post.model');

const formidableOptions = {
    filter: function ({ name, originalFilename, mimetype }) {
        return mimetype && mimetype.includes("image");
    },
    maxFileSize: 5 * 1024 * 1024
};

const createPost = (req, res) => {
    var form = formidable(formidableOptions);
    form.parse(req, async (err, fields, files) => {
        if (err) return res.status(500).send();
        const postProps = { featured: false, ...fields }
        postProps.date = new Date();
        if (!files.picture || !postProps.title || !postProps.content) {
            return res.status(400).send({ message: "Missing post attributes" });
        }
        postProps.picture = await Files.saveFile(files.picture, 'images');
        if (files.author_pic)
            postProps.author_pic = await Files.saveFile(files.author_pic, 'images');
        const post = await Posts.create(postProps)
        res.send(post.toJSON());
    });
}

const updatePost = async (req, res) => {
    const post = await Posts.findById(req.params.id);
    var form = formidable(formidableOptions);
    form.parse(req, async (err, fields, files) => {
        if (err) return res.status(500).send();
        post.title = fields.title;
        post.content = fields.content;
        post.author = fields.author;
        post.featured = fields.featured;

        if (files.picture)
            post.picture = await Files.saveFile(files.picture, 'images');
        if (files.author_pic)
            post.author_pic = await Files.saveFile(files.author_pic, 'images');

        await post.save();
        res.send(post.toJSON());
    });
}

const getAll = async (req, res) => {
    const { page = 1, items = 10, featuredFirst = false, featured } = req.query;
    try {
        const order = [];
        if (featuredFirst) order.push(['featured', -1])
        order.push(['date', -1]);
        const filter = {};
        if (featured) filter.featured = 1
        const posts = await Posts.find(filter)
            .sort(order)
            .skip((page - 1) * items)
            .limit(parseInt(items));
        for (let post of posts)
            post.content = post.content.substring(0, 300) + (post.content.length > 300 ? '...' : '');
        const count = await Posts.countDocuments(filter);
        const result = { items: posts, count };
        if (req.query.count_featured)
            result.featured = await Posts.countDocuments({ featured: true });
        res.send(result)
    } catch (e) {
        console.log(e);
        res.status(500).send({ message: "an error occured" })
    }

}

const getPost = (req, res) => {
    Posts.findById(req.params.id)
        .then((post) => {
            if (post)
                return res.send(post)
            res.status(400).send({ message: 'Post not found' })
        })
}

const deletePost = (req, res) => {
    Posts.findById(req.params.id)
        .then((post) => {
            if (!post)
                return res.status(400).send({ message: "Post not found" })

            Posts.deleteOne({ _id : post._id })
                .then(() => res.send({ message : "Post deleted" }));
        })
        .catch(() => res.status(500).send({ message : "Error occured" }));
}

exports.PostsCotroller = {
    createPost,
    deletePost,
    getAll,
    getPost,
    updatePost
}